import 'package:flutter/material.dart';
import 'inherited_widgets/note_inherited_widget.dart';
import 'screens/notes_list.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NoteInheritedWidget(
      MaterialApp(
        theme:
        ThemeData(primarySwatch: Colors.blue, brightness: Brightness.light),
        themeMode: ThemeMode.dark,
        darkTheme: ThemeData(brightness: Brightness.dark),
        home: NotesList(),
      ),
    );
  }
}
