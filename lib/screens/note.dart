import 'package:flutter/material.dart';
import 'package:notesapp/components/rounded_button.dart';
import 'package:notesapp/inherited_widgets/note_inherited_widget.dart';
import 'package:notesapp/providers/note_provider.dart';

enum NoteMode { Adding, Editing }

class Note extends StatefulWidget {
  final NoteMode noteMode;
  final Map<String, dynamic> note;
  Note(this.noteMode, this.note);

  @override
  _NoteState createState() => _NoteState();
}

class _NoteState extends State<Note> {
  final TextEditingController _titlecontroller = TextEditingController();
  final TextEditingController _textcontroller = TextEditingController();

  List<Map<String, String>> get _notes => NoteInheritedWidget.of(context).notes;

  @override
  void didChangeDependencies() {
    if (widget.noteMode == NoteMode.Editing) {
      _titlecontroller.text = widget.note['title'];
      _textcontroller.text = widget.note['text'];
    }

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      backgroundColor: Colors.cyan.shade50,
      appBar: AppBar(
        title:
            Text(widget.noteMode == NoteMode.Adding ? "Add Note" : "Edit Note"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(40.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: _titlecontroller,
              decoration: InputDecoration(hintText: "Enter Note Title"),
            ),
            SizedBox(
              height: 20.0,
            ),
            TextField(
              controller: _textcontroller,
              decoration: InputDecoration(hintText: "Enter Note Text"),
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _NoteButton("Save", Colors.lightBlueAccent, () {
                  final title = _titlecontroller.text;
                  final text = _textcontroller.text;
                  if (widget?.noteMode == NoteMode.Adding) {
                    NoteProvider.insertNote({'title': title, 'text': text});
                  } else if (widget?.noteMode == NoteMode.Editing) {
                    NoteProvider.updateNote({
                      'id': widget.note['id'],
                      'title': _titlecontroller.text,
                      'text': _textcontroller.text,
                    });
                  }
                  Navigator.pop(context);
                }),
                _NoteButton("Discard", Colors.blueGrey, () {
                  Navigator.pop(context);
                }),
                widget.noteMode == NoteMode.Editing
                    ? _NoteButton("Delete", Colors.red, () async {
                        await NoteProvider.deleteNote(widget.note['id']);
                        Navigator.pop(context);
                      })
                    : Container(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _NoteButton extends StatelessWidget {
  final String text;
  final Color color;
  final Function onPressed;
  _NoteButton(this.text, this.color, this.onPressed);
  @override
  Widget build(BuildContext context) {
    return RoundedButton(
      colour: color,
      title: text,
      onPressed: onPressed,
    );
  }
}
