import 'package:flutter/material.dart';
import 'package:notesapp/providers/note_provider.dart';
import 'package:notesapp/screens/note.dart';

class NotesList extends StatefulWidget {
  @override
  _NotesListState createState() => _NotesListState();
}

class _NotesListState extends State<NotesList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      backgroundColor: Colors.cyan.shade50,
      appBar: AppBar(
        title: Text("Notes App."),
      ),
      body: FutureBuilder(
        future: NoteProvider.getNoteList(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            final notes = snapshot.data;
            return ListView.builder(
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                Note(NoteMode.Editing, notes[index])));
                  },
                  child: Card(
                    elevation: 15.0,
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 20.0, bottom: 20.0, left: 15.0, right: 15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          _NotesTitle(notes[index]['title']),
                          SizedBox(
                            height: 10.0,
                          ),
                          _NotesText(notes[index]['text']),
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: notes.length,
            );
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => Note(NoteMode.Adding, null)));
          },
          child: Icon(Icons.add)),
    );
  }
}

class _NotesTitle extends StatelessWidget {
  final String _title;
  _NotesTitle(this._title);
  @override
  Widget build(BuildContext context) {
    return Text(
      _title,
      style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25.0),
    );
  }
}

class _NotesText extends StatelessWidget {
  final String _text;
  _NotesText(this._text);
  @override
  Widget build(BuildContext context) {
    return Text(
      _text,
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        fontSize: 15.0,
      ),
    );
  }
}
